//////////////////////////////////////////////////////////////////////////////
// FILE   : IpoptCore.tol
// PURPOSE: Interior Point Optimization core package
//////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////
NameBlock IpoptCore =
//////////////////////////////////////////////////////////////////////////////
[[

//read only autodoc
Text _.autodoc.name = "IpoptCore";
Text _.autodoc.brief = "Interior Point Optimization core package";
Text _.autodoc.description = 
"Loads Interior Point Optimization library (IPOPT) to be used with C and "
"Cpp. This package is just an store of binaries used in TolIpopt and have no "
"relevant member nor method. The target is to minimize the flux and the total "
"size in the repository, due to original binaries of IPOPT will be change "
"about one or two times by year while TOL source code of TolIpopt could be "
"modified each month one or more times in order to implement new features.\n"
"More details at <a href=\""
"https://projects.coin-or.org/Ipopt"
"\" target=\"_blank\"> IPOPT home page</a>n"
"Current available version of IPOPT in this port is 3.9.1 \n";
Text _.autodoc.url = 
"http://packages.tol-project.org/OfficialTolArchiveNetwork/repository.php";
Set _.autodoc.keys = [["non-linear", "local", "interior point", "optimization"]];
Set _.autodoc.authors = [[ "vdebuen@tol-project.org"]];
Text _.autodoc.minTolVersion = "v2.0.1 b.0.56.alpha";
Real _.autodoc.version.high = 3;
Real _.autodoc.version.low = 10;
Set _.autodoc.dependencies = Copy(Empty);
Set _.autodoc.nonTolResources = [[
  Text "Windows_x86_32"
]] ;
Text _.autodoc.versionControl = AvoidErr.NonDecAct(OSSvnInfo("."));

NameBlock CppTools = [[ Real unused = ?]];

Real _is_started = False;

////////////////////////////////////////////////////////////////////////////
Real StartActions(Real void)
////////////////////////////////////////////////////////////////////////////
{
  If(Or(_is_started,OSUnix), False, {
    Real _is_started := True;
    Text prefix = If( OSUnix, "lib", "" ) + "ipopt39";
    Text dllPath.rel = GetAbsolutePath(GetHardSoftPlattform(0));
    Text dllPath.deb = dllPath.rel+"_dbg";
    Text dllFile.rel = dllPath.rel+"/"+prefix+"."+GetSharedLibExt(0);
    Text dllFile.deb = dllPath.deb+"/"+prefix+"."+GetSharedLibExt(0);
    Text dllFile = If(!TextEndAt(Version,"{DEBUG}"),dllFile.rel,
    {
      If(OSDirExist(dllFile.deb),dllFile.deb,
      {
        WriteLn("Cannot find debug library "+dllFile.deb+
                ".\n Trying to load "+dllFile.rel,"W");
        dllFile.rel
      })
    });
    Real ok = LoadDynLib( dllFile );
    WriteLn("[IpoptCore::StartActions] Loading library "+dllFile+
            " -> "<<If(ok,"OK","FAIL"));
    ok
  })
}

]];

